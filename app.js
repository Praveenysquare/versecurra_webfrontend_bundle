const express = require('express');
const app = express();
const path=require('path')
const PORT = 9000
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios')
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname,'build')));
const queryString = require('query-string');


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname,'build','index.html'));
  
});


app.get('/chat', function (req, res) {
    const { name, room } = req.query
    console.log("data1", name, room, req.query)
    res.sendFile(path.join(__dirname,'build','index.html'));
  
});





app.get('/session', function (req, res) {
    // console.log(req.username);
    const { username, session_id } = req.query
    // console.log("data1", username, session_id, req.query)
    // res.render('build', { username, session_id});
    res.json({ username, session_id});
});






module.exports = app;
app.listen(PORT, () => {
   console.log(PORT, ' is the magic port');
})
